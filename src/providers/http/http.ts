import { HttpClient,HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {MainHttpProvider} from '../main-http/main-http';
import {SharedService} from '../global';
// import  = require("");

/*

  Generated class for the HttpProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class HttpProvider {

   data:any;
	 code:any;

  constructor(public http: HttpClient,public mainHttp:MainHttpProvider,public globalVar:SharedService ) {
    console.log('Hello HttpProvider Provider');
  }

	

  //defining login function to hit login API
  login (data){
  //constructing req_data object and using url from global file
    var req_data = {
      "url":this.globalVar.login_url,
      "headers":{
        "Content-type":"application/json"
      },
      "method":"GET",
      "params":{
        usr:data.username,
        pwd:data.password
      }
    }
    //calling API from mainhttp service by passing req_data as param 
    return this.mainHttp.api_call(req_data)
  
  }

  //defining getEvents function
  getEvents(){
    //constructing req_data object and using params from global file
    var req_data = {
      "url":"",
      "method":"GET",
      "params":this.globalVar.event_list
    }
      //returning calling API from mainhttp service by passing req_data as param
     return this.mainHttp.api_call(req_data)
  }

  getEventDate(event){
    this.globalVar.get_event_date.value = event;
    var req_data = {
      "url":"",
      "method":"GET",
      "params":this.globalVar.get_event_date
    }
       //returning and calling API from mainhttp service by passing req_data as param
    return this.mainHttp.api_call(req_data)
  }

  //defining addAttendance by passing event, date, name, id as params
  addAttendance(event,date,name,ID){
  //using save_attendance object from globalfile
    console.log(this.globalVar.save_attendance)
    //assigning values
    this.globalVar.save_attendance.doc.event=event;
    this.globalVar.save_attendance.doc.event_date=date;
    this.globalVar.save_attendance.doc.member_name=name;
    this.globalVar.save_attendance.doc.member=ID;
    // initisizing a variable data and storng object as FORMDATA
    var data = this.objectToFormData(this.globalVar.save_attendance)
    console.log(data);
    //constructing req_object for POSTMETHOD
     var req_data = {
      "url":"",
      "method":"POST",
      "params":"",
      "body": data
    }

    //returning and calling API from mainhttp service by passing req_data as param
    return this.mainHttp.api_call(req_data)
  }


  objectToFormData = function(obj, form?, namespace?) {
    
   const searchParams = Object.keys(obj).map(key => {
        if(key == 'doc'){
            return key + '=' + JSON.stringify(obj[key]);
        }else{
            return key + '=' + obj[key];
        }

      }).join('&');

   return searchParams
  };

 



}