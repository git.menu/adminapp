import { Injectable } from '@angular/core';

@Injectable()
export class SharedService {
  URL:string = "http://40.76.21.162:8002/"; 
  login_url = "api/method/login";

  headers = {
  	"Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
  }

  event_date ="";
  event_name=""; 
  member_id="NPO-MEM-2019-00006"; 
  member_name="Jyothi A";
  image="";
  discription="";
  time_stamp:any= new Date().getTime();

  //get event list --Get

  event_list = {
  	txt: "",

	doctype: "Events",

	ignore_user_permissions: 0,

	reference_doctype: "Member Attendance",

	cmd: "frappe.desk.search.search_link",

	_: this.time_stamp

  }





  //Get event date --Get
  get_event_date = {
  	value: "EVENT-00004",

	options: "Events",

	fetch: ["event_date","event_name","image","discription"],


	cmd: "frappe.desk.form.utils.validate_link",

	_: this.time_stamp
  }

  // Members list  ---GEt

  members_list = {
  	doctype: "Member",

	ignore_user_permissions: 0,

	reference_doctype: "Member Attendance",

	cmd: "frappe.desk.search.search_link",

	_: this.time_stamp
  }

  // Save attendance ---POst

  	save_attendance = {
  		doc:{
			"docstatus": 0,
			"doctype": "Member Attendance",
			"name": "New Member Attendance 1",
			"__islocal": 1,
			"__unsaved": 1,
			"owner": "Administrator",
			"series": "NPO-ATD-.YYYY.",
			//"naming_series": "NP-ATD-.YYYY.",
			"status": "Present",
			"member_name": this.member_name,
			"member": this.member_id,
			"event_name": null,
			"event_date": this.event_date,
			"event": this.event_name
		},

	action: "Save",

	cmd: "frappe.desk.form.save.savedocs"

  	}

}