import { HttpClient,HttpHeaders,HttpParams } from '@angular/common/http';
import { RequestOptions,URLSearchParams,Headers } from '@angular/http';
// import { Http, Headers, RequestOptions } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { Injectable } from '@angular/core';
//it is imported for url and params,objects which  are stored locally
import  {SharedService} from "../global";


/*
  Generated class for the MainHttpProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MainHttpProvider {

  //initisalizing
	BASE_URL="";
	header_data:any;

  constructor(public http: HttpClient,public globalVar:SharedService) {
  	console.log('Hello MainHttpProvider Provider');
    //assigning url to baseurl
  	this.BASE_URL=this.globalVar.URL;
  }

  //defining API call and passing data
  api_call (data:any) {
    //condition checking  
  	if(data.method=="GET"){
    //constructing headers
  		const headers =new HttpHeaders({
	  	  	'Content-Type': 'application/json'

	  	 });

       //constructing options
  		const options = {
  			headers:headers,
  			params:data.params
  		}
  		//calling http GET method by passing params url,headers,options and mapping response
  		return this.http.get(this.BASE_URL+data.url,options).map((resp)=>{
      //printing response
  			console.log(resp)
        //return response
  			return resp;
  		},(error)=>{
        //if call failed it will show alert
  			alert("Un Authorised Access")
  		})
      //if is it post it will come to else condition
  	}else if(data.method=="POST"){
      //constructing headers by using headers stored in global file(importing post headers from global file)
  		const HEADERS = new HttpHeaders(this.globalVar.headers);
  		//constructing options
  		const options = {
  			headers:HEADERS,
  			params:data.params
  		}
      //calling http POST method by passing params as url,options dsts.body and mapping response
  		return this.http.post(this.BASE_URL+data.url,data.body,options).map((resp)=>{
        //return response
  			return resp;
  		},(error)=>{
      //if call failed it will show alert
  			alert("Un Authorised Access")
  		})
      //if method is wrong or not specified it show alert
  	}else {
  		alert('INVALID Method...')
  	}


  }




}
