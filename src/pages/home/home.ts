import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { BarcodeScanner,BarcodeScannerOptions } from '@ionic-native/barcode-scanner';

import { LoginPage } from '../login/login';
import { HttpProvider } from '../../providers/http/http';

import {SharedService} from '../../providers/global';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
   
})
export class HomePage {

  qrData = null;  
  createdCode = null;
  scannedCode = null;
  ScanResult= "8101375844473831341747384833441"
  Result:any;
  selectedEvent:any;
  eventDate:any;
  event_list:any =[];
  memberID="NPO-MEM-2019-00004"
  memberName="Ram V";
  event_name="EVENT-00001";
  event_date = "2019-01-31"
  image="";

  public buttonClicked: boolean = false;

  options :BarcodeScannerOptions;
   
  constructor(public navCtrl: NavController,public httpProvider: HttpProvider,
    private barcodeScanner: BarcodeScanner,
    public globalVar:SharedService) {


  }
 




 ///Opening QR code scanner
  scanCode() {
    this.options = {
        preferFrontCamera : true, 
        showFlipCameraButton : true,
        prompt:"Place a barcode inside the scan area"
    }


    this.barcodeScanner.scan(this.options).then(barcodeData => {
      this.scannedCode = JSON.parse(barcodeData.text);
     this.httpProvider.addAttendance(this.event_name,this.event_date, this.scannedCode['name'], this.scannedCode['id']).subscribe(data=>{
       alert("Attendance saved..")
        
      });

    }, (err) => {
        console.log('Error: ', err);
    });



   
     this.scannedCode={"name":"Ram V", "id":"NPO-MEM-2019-00004"}
    //this.scannedCode={"name":"Jyothi A", "id":"NPO-MEM-2019-00006"}
   console.log(this.scannedCode);
    console.log(this.scannedCode['name']);
    alert(this.scannedCode['id']);

    this.httpProvider.addAttendance(this.event_name,this.event_date, this.scannedCode['name'], this.scannedCode['id']).subscribe(data=>{
    alert(data);
    console.log(data);
    alert("Attendance saved..")
        
   });



   
  }


  getEventList () {

    this.httpProvider.getEvents().subscribe(data=>{
      console.log(data);
      try{
        for(let i=0;i < data["results"].length;i++){
          this.event_list.push(data["results"][i].value)
        }

        console.log(this.event_list)
      }
      catch (e){
        alert(e)
      }
    });
  }

  getDate() {
    console.log(this.selectedEvent)

    this.httpProvider.getEventDate(this.selectedEvent).subscribe(data=>{

      console.log(data);

      this.globalVar.event_name = this.selectedEvent;
       this.globalVar.image = data['fetch_values'][3];
      this.globalVar.event_date = data['fetch_values'][1];
      this.eventDate= data['fetch_values'][1];
      console.log(this.eventDate)
      // try{
      //   for(let i=0;i < data.results.length;i++){
      //     this.event_list.push(data.results[i].value)
      //   }

      //   console.log(this.event_list)
      // }
      // catch (e){
      //   alert(e)
      // }
    });
    this.buttonClicked = !this.buttonClicked;
  }

 

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
    this.getEventList()
  }


}
