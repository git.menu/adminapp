import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController,LoadingController } from 'ionic-angular';


import { HomePage } from '../home/home';
import { HttpProvider } from '../../providers/http/http';

import {  SharedService } from '../../providers/global';


/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
  
})
export class LoginPage {
  //declaring and assigning values for URL,username,password
  URL:any="";
  username:any="ramarao.vch@gmail.com";
  password:string="pavani@123";
	
  //using neccessary dependency in constructor
  constructor(public navCtrl: NavController, public navParams: NavParams,public httpProvider: HttpProvider,public globalVar:SharedService,  public alertCtrl: AlertController,public loadingCtrl: LoadingController) {
    //importing url from global.ts file and storing. 
    this.URL = this.globalVar.URL;
    
  }

  //defining login function
    goToLogin(){
      this.globalVar.URL = this.URL;
      var data = {
        username:"ramarao.vch@gmail.com",
        password:"pavani@123"

      }
      let loader = this.loadingCtrl.create({
            content: "Please wait...",
            duration: 1500
          });
    loader.present();
      //calling function in servicelayer and passing data as param
      this.httpProvider.login(data).subscribe(data => {
      
           let result=data;
          console.log(result);
          let Message =result['message'];
          console.log(Message);
          if (Message =="Logged In") {
          //navigating to homepage
          this.navCtrl.push(HomePage);
          }
         else{
       let alert = this.alertCtrl.create({
      title: 'Sorry!',
      subTitle: 'Login Failed due to invalid credentials',
      buttons: ['OK']
      });
      alert.present();
      }
    },(err)=>{
      let alert = this.alertCtrl.create({
      title: 'Error!',
      // subTitle: err,
      subTitle : "Invalid Credentials",
      buttons: ['OK']
    });
    alert.present();
        console.log(err);
        return err;
      })
    
    
}

 


  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

}
